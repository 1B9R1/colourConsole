const bgColorCode = {
  'black': 40,
  'red': 41,
  'green': 42,
  'yellow': 43,
  'blue': 44,
  'purple': 45,
  'white': 47
}

const fontColorCode = {
  'black': 30,
  'red': 31,
  'green': 32,
  'yellow': 33,
  'blue': 34,
  'purple': 35,
  'white': 37
}

/**
 * 美化 console.log 输出
 * @param {Object[]} list
 * @param {string} list[].color - 字体颜色
 * @param {string} list[].bg - 背景色
 * @param {string} list[].text - 内容
 * @returns {string} 格式化后的字符串
 */
module.exports = (list) => {
  let result = ''

  list.forEach((obj) => {
    const color = obj.color || 'white'
    const bg = obj.bg || 'black'
    const colorCodeForFont = fontColorCode[color] || 37
    const colorCodeForBg = bgColorCode[bg] || 40

    result += `\033[${colorCodeForBg};${colorCodeForFont}m` + obj.text
  })

  result += '\033[0m'

  return result
}
对服务端的 `console.log` 进行着色处理。
原理可以看[这篇文章](https://www.jianshu.com/p/cca3e72c3ba7)

### 使用方法
```javascript
const colourConsole = require('path/to/colourConsole.js')
const src = [
  {
    text: 'Hello ',
    color: 'yellow'
  }, {
    text: 'World!',
    bg: 'yellow'
  }
]
const result = colourConsole(src)

console.log(result)
```

### 支持的颜色
- black - 背景默认色
- white - 字体默认色
- red
- green
- yellow
- blue
- purple

*PS：在浏览器的控制台中输出无效，需要在 cmd 环境中输出*